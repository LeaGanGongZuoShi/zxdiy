export const citiest = [
  { "label": "北京Beijing010", "name": "北京", "pinyin": "Beijing", "zip": "010" },
  { "label": "重庆Chongqing023", "name": "重庆", "pinyin": "Chongqing", "zip": "023" },
  { "label": "上海Shanghai021", "name": "上海", "pinyin": "Shanghai", "zip": "021" },
  { "label": "天津Tianjin022", "name": "天津", "pinyin": "Tianjin", "zip": "022" },
  { "label": "长春Changchun0431", "name": "长春", "pinyin": "Changchun", "zip": "0431" },
  { "label": "长沙Changsha0731", "name": "长沙", "pinyin": "Changsha", "zip": "0731" },
  { "label": "常州Changzhou0519", "name": "常州", "pinyin": "Changzhou", "zip": "0519" }]