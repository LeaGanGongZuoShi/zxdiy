// pages/manage/nav/index.js
var wxCharts = require('../../../utils/wxcharts.js');
var app = getApp();
var lineChart = null;
const db = wx.cloud.database()
const _ = db.command
const util = require("../../../utils/util.js");

Page({
  data: {
    width: 180,
    height: 400,
    newuser:0,
    visituser:0,
    alluser:0,
    allvisituser:0
  },
  touchHandler: function (e) {
    console.log(lineChart.getCurrentDataIndex(e));
    lineChart.showToolTip(e, {
      // background: '#7cb5ec',
      format: function (item, category) {
        return category + ' ' + item.name + ':' + item.data
      }
    });
  },
  onLoad: function (e) {
    let day = util.formatTime(new Date()).split(" ")[0];
    console.log(day)

    db.collection('user_list').count().then(res => {
      this.setData({
        alluser:res.total
      })
    })
    db.collection('user_info').count().then(res => {
      this.setData({
        allvisituser: res.total
      })
    })
    db.collection('user_info').where({
      createtime: _.gte(day)
    }).count().then(res => {
      this.setData({
        visituser: res.total
      })
    })
    db.collection('user_list').where({
      createtime: _.gte(day)
    }).count().then(res => {
      this.setData({
        newuser: res.total
      })
    })

    var windowWidth = 320;
    try {
      var res = wx.getSystemInfoSync();
      windowWidth = res.windowWidth - 20;
      this.setData({
        width: res.windowWidth,
        height: res.windowHeight
      })
      console.log(windowWidth)
    } catch (e) {
      console.error('getSystemInfoSync failed!');
    }
    var categories = [];
    var data = [];
    var dataf = [];
    var ti = -5;
    var t1 = ti;
    var t2 = ti;
    var flag1 = true;
    var flag2 = true;
    for (var i = ti; i < 1; i++) {
      categories.push(util.getDay(i));
    }
    
    let timer1 = setInterval(function () {
      console.log("timer1")
      if(flag1){
        flag1 = false;
        db.collection('user_info').where({
          createtime: _.gte(util.getDay(t1))
        }).count().then(res => {
          dataf.push(res.total);
          t1++;
          flag1 = true;
          if (t1 == 1) {
            clearInterval(timer1);
          }
        });
      } 
    },100);
    let timer2 = setInterval(function () {
      console.log("timer2")
      if(flag2){
        flag2 = false;
        db.collection('user_list').where({
          createtime: _.gte(util.getDay(t2))
        }).count().then(res => {
          data.push(res.total);
          t2++;
          flag2 = true;
          if (t2 == 1) {
            clearInterval(timer2);
          }
        })

      }
        
    },100);

    let timer = setInterval(function () {
      if (t1 == 1&&t2==1) {
        clearInterval(timer);
        lineChart = new wxCharts({
          canvasId: 'lineCanvas',
          type: 'line',
          categories: categories,
          animation: true,
          // background: '#f5f5f5',
          series: [{
            name: '新用户',
            data: data,
            format: function (val, name) {
              return val.toFixed(2) + '个';
            }
          }, {
            name: '访客数',
            data: dataf,
            format: function (val, name) {
              return val.toFixed(2) + '个';
            }
          }],
          xAxis: {
            disableGrid: true
          },
          yAxis: {
            title: '数量 (个)',
            format: function (val) {
              return val.toFixed(2);
            },
            min: 0
          },
          width: windowWidth,
          height: 200,
          dataLabel: false,
          dataPointShape: true,
          extra: {
            lineStyle: 'curve'
          }
        });
      }
    }, 100)

  }
})