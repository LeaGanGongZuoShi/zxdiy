const db = wx.cloud.database()
const product = db.collection('pictures')
const _ = db.command
const dataPageNum = 6
Page({
  data: {
    color: wx.getExtConfigSync().color,
    rankList: ["图标", "图片"],
    show_type: 0, // 0：块状 1：列表
    orderType: 0, // 0："desc"降序   1："asc"升序
    orderBy: 'createtime', // "rank":综合 "sale":销量 "price":价格
    selectIndex: 0,
    search_text: '',
    product_list: [],
    is_all: 0,
    page: 1,
    dataNum: 0,
    isSearch: 0,
    loadCount: 0,
    showCount: 0
  },
  onLoad: function (options) {
    this.setData({
      loadCount: this.data.loadCount + 1
    })
    if (options.text) {
      this.setData({
        search_text: options.text
      })
    }
  },
  onShow: function () {
    console.log(345)
    this.setData({
      showCount: this.data.showCount + 1
    })
    if (this.data.showCount == this.data.loadCount) {
      this._clear();
      this.setData({
        dataNum: 0
      })
      this.getProducts(0);
    }
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];
    if (currPage.data.req == 1) {
      this._clear();
      this.setData({
        dataNum: 0
      })
      this.getProducts(0);
      this.setData({
        req: 0
      })
    }
  },
  _clear() {
    this.setData({
      page: 1,
      product_list: [],
      is_all: 0,
      show_type: 0,
      orderType: 0,
      orderBy: 'name'
    })
  },
  bindconfirm(e) {
    this.data.search_text = e.detail;
    this._clear();
    this.getProducts(0);
  },
  getProducts(e) {
    let product_list = this.data.product_list;
    if (this.data.isSearch == 1)
        this.setData({
          dataNum: 0
        })
      if (e == 3) {
        this.setData({
          dataNum: 0
        })
      }
    
    db.collection('pictures').where({ type: this.data.selectIndex}).orderBy(this.data.orderBy, this.data.orderType ? 'asc' : 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
        console.log(res.data);
        this.data.page++;
        const DATA = res.data;
        product_list = product_list.concat(DATA);
        this.setData({
          dataNum: this.data.dataNum + dataPageNum
        })
        if (product_list.length != this.data.dataNum) this.data.is_all = 1;
        this.setData({
          page: this.data.page,
          is_all: this.data.is_all,
          product_list: product_list,
          orderType: this.data.orderType
        });
      })
      this.setData({
        isSearch: 0
      })
    

  },
  // 切换商品展示状态
  switch_type(e) {
    const type = e.currentTarget.dataset.type;
    // show_type 0：块状 1：列表
    this.setData({
      show_type: type
    });
  },
  changeRank(e) {
    const index = Number(e.currentTarget.dataset.index);
    const type = Number(e.currentTarget.dataset.order_type);
    console.log(index)
    console.log(type)
    this.setData({
      page: 1,
      product_list: [],
      is_all: 0,
      selectIndex: index
      })
    this.getProducts(3);
  },
  goProductDetail: function (e) {
    console.log(e.currentTarget.dataset.id);

    wx.navigateTo({

      url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.id}`
    });
  },
  onPullDownRefresh: function () {
    setTimeout(() => {
      this._clear();
      this.getProducts(1);
      wx.stopPullDownRefresh();
    }, 500)
  },
  onReachBottom: function () {
    if (!this.data.is_all) {
      this.getProducts(2);
    }
  },
  onAddProduct: function (e) {
    wx.navigateTo({
      url: `/pages/manage/product-add/index`
    });
  },
  onDelPro(e) {
    console.log(e.currentTarget.dataset.id);
    wx.cloud.callFunction({
      // 云函数名称
      name: 'product-del',
      // 传给云函数的参数
      data: {
        id: e.currentTarget.dataset.id
      },
      complete: res => {
        if (res.errMsg == "cloud.callFunction:ok") {
          wx.showToast({
            title: '数据已被删除',
          })
          this._clear();
          this.setData({
            dataNum: 0
          })
          this.getProducts(0);

        } else
          wx.showToast({
            title: '删除失败，请稍后再试',
            icon: 'none'
          })
        console.log('callFunction test result: ', res)
      }
    })
    // wx.navigateTo({

    //   url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.id}`
    // });
  },
  onUptPro(e) {
    console.log(e.currentTarget.dataset.id);
  },
  onGoodsSelect(e) {

    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];  //当前页面
    var prevPage = pages[pages.length - 2]; //上一个页面
    let goodsInfo = { url: e.currentTarget.dataset.index}
    //直接调用上一个页面的setData()方法，把数据存到上一个页面中去
    prevPage.setData({
      imgSelected: goodsInfo
    })

    wx.navigateBack();
  }
})